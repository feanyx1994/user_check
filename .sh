# user_check
In a cPanel server, slaps together a quick user_check function in RAM. Point of the below is to gen the IP addresses, doc roots, all that good stuff, but it takes a snapshot of ps faux, and saves it to a temp file, so after you're done fixing what's broken, you'll have a better idea of what broke it, for documentation purposes. 
user() 
#Starts the function.  A function is basically a list of commands to be run. So, that user section tells RAM "hey buddy, you got a variable coming your way, call it $1, since theres only the one." You can have more then one function in a variable, but we're only dealing with a single linux user  at a time ¯\_(ツ)_/¯
{
sudo cat /usr/local/apache/logs/error_log | grep YOUR_IP_GOES_HERE 
sudo tail -f /var/log/nginx/access.log | grep YOUR_IP_GOES_HERE
#Greps out your IP address against the Apache then NGINX Log. Checks for errors while you're loading the site
#gens the local server IP address. 
hostname -i
ps faux | grep $1 | cat >> $1_PS_FAUX_OUTPUT
#Runs the process list, and lists off the file directorys, user and process IDs. Saves that to a temp file
cat /etc/redhat-release && /usr/local/cpanel/cpanel -V
Gives both the current RHEL/Centos ver and the current cPanel ver. Helps for troubleshooting ver-centric glitches
df
Shows free disk space. Linux won't stop you from filling up the disk. 
awk '{print $1}'  /home/$1/.lastlogin
#cPanel saves the last IP addresses, this parses out just the initial collumn of last known logged in cPanel IP addresses. 
for i in $(ls /home/$1/public_html/wp-content/plugins/); do echo $i; done
# lists off the contents of the users wp-content/plugins. Gives a quick n dirty idea of what software their WP site runs, and also by-proxy tells you whether they're running WP or not, since no other CMS uses a wp-*****/ file structure
switch $1 ; cat $1_PS_FAUX_OUTPUT ; rm -rf $1_PS_FAUX_OUTPUT ; ps faux | grep $1
#Switches into the user to be troubleshot, cats out the temp folder, then removes it, and runs a process list for that user, to compare what was going on prior to the (hopefully) successfull troubleshooting
}

